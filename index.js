const selectedNotes = new Set();


let getActiveDots = () => {
  return $('span.note-dot').filter(
    (index, element) => {
      return selectedNotes.has($(element).data('note'));
    });
};


let onChordPadPress = (clickEvent) => {
  getActiveDots().each(
      (index, element) => {
        playNote($(element).data('note'));
      });
};


let onClear = () => {
  selectedNotes.clear();
  updateChordPadColors();
};


let onTonePadPress = (clickEvent) => {
  let note = $(clickEvent.target).data('note');
  playNote(note);
  selectedNotes.add(note);
  updateChordPadColors();
};


let playNote = (note) => {
  new Audio('/notes/' + note + '.mp3').play();
};


let updateChordPadColors = () => {
  $('span.note-dot').removeClass('on');
  getActiveDots().addClass('on');
};


let init = () => {
  $('button.pad#chord').click(onChordPadPress);
  $('button.pad.tone').click(onTonePadPress);
  $('button#clear').click(onClear);
};


$(document).ready(init);
